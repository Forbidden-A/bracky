# Bracky

[![License](https://img.shields.io/badge/Licensed%20under-GPLv3-red.svg?style=flat-square)](./LICENSE) 
[![Made with Python](https://img.shields.io/badge/Made%20with-Python-ffde57.svg?longCache=true&style=flat-square&colorB=ffdf68&logo=python&logoColor=88889e)](https://www.python.org/) 
[![Using lightbulb](https://img.shields.io/badge/Using-lightbulb-ffde57.svg?longCache=true&style=flat-square&colorB=4584b6&logo=discord&logoColor=7289DA)](https://github.com/tandemdude/hikari-lightbulb/) 
[![Powered by Hikari](https://img.shields.io/badge/Powered%20by-Hikari-ff69b4.svg?longCache=true&style=flat-square)](https://github.com/hikari-py/hikari) 
[![Server](https://img.shields.io/discord/265828729970753537.svg?style=flat-square&label=DCA)](https://discord.gg/RDKfMcC)

The guild bot for the [**Discord Coding Academy**](https://discord.gg/RDKfMcC) server.

It serves a multitude of purposes, ranging from saying good bye to leaving users to managing the
bots we have. All implemented features come out of a necessity, and even though there are bots that
could fill some of our needs, it wouldn't be as cool as implementing them ourselves :P

Here follows a more comprehensive list of all functionality currently implemented:

- Welcomer (and "farweller")
- Bump warden (reminders for server list bumping, like Disboard)
- Self assignable roles
- Role menus (with reactions)
- Bot management system (automatic invites, kicks, nick prefixes and ownerships)


## Contributing

Contributions are fundamental to any project of this nature, so naturally we really appreciate
them.
Contributing doesn't just mean making actual source changes; helping in reviews and participating in
issue discussions is really valueable and appreciated!

#### Contributing feedback

If you feel like contributing but don't want to venture out into the codebase, you can help us make
decisions about the project and review pending source contributions. 

There are plenty of [issues](dcacademy/bracky>/issues) that need to be discussed, so feel free to
select one of them and spark the conversation.  
You can also take a look at the open [merge request](dcacademy/bracky>/merge_requests) and give your
opinion on some changes.

#### Contributing code 

Start by [picking an issue](dcacademy/bracky>/issues) (or open a new one) and ask a
maintainer to assign it to yourself. Then fork the project (if you haven't already), checkout to
`dev` and branch off from there. After making your changes and committing, [open a merge
request](dcacademy/bracky>/merge_requests/new) on this repository, putting your fork's branch as the
source and our `dev` as target. Make sure your merge request description is thorough enough and
clearly covers the changes you made. After that just wait for someone to review your request, reply
and fix any discussed issues.


## License

This project is licensed under the [GNU General Public License version
3](https://www.gnu.org/licenses/gpl-3.0.html) and our copy of the license can be found in the
[COPYING.md](COPYING.md) file.
