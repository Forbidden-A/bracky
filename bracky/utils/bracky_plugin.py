#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

__all__ = ["BrackyPlugin"]

import inspect
import os
import pathlib
import typing

import lightbulb

from bracky.config import get_config


class BrackyPlugin(lightbulb.Plugin):
    def __init_subclass__(cls):
        super().__init_subclass__()
        cls.category: str = pathlib.Path(inspect.getfile(cls)).parent.stem

    def __init__(self, bot: lightbulb.Bot, category: typing.Optional[str] = None, name: typing.Optional[str] = None):
        super().__init__(name=name)
        self.app_config = get_config()
        self.bot = bot
