#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import random
import typing

import hikari
import lightbulb

from bracky import config
from bracky.utils import BrackyPlugin


class Welcomer(BrackyPlugin):
    def __init__(self, bot: lightbulb.Bot):
        super().__init__(bot)
        self.config_cache: typing.Optional[config.WelcomerConfig] = None
        self.channel: typing.Optional[hikari.GuildTextChannel] = None

    def maybe_load_config(self):
        if self.config_cache is None:
            if self.app_config.welcomer is not None:
                self.config_cache = self.app_config.welcomer

    @lightbulb.listener(hikari.MemberDeleteEvent)
    async def welcomer_on_member_leave(self, event: hikari.MemberDeleteEvent):
        self.maybe_load_config()

        if self.config_cache is None or not self.config_cache.leave_messages:
            return

        msg = random.choice(self.config_cache.leave_messages)

        if self.channel is None:
            self.channel = event.app.cache.get_guild_channel(self.config_cache.channel_id)

        await self.channel.send(msg.format(user=event.user))


def load(bot: lightbulb.Bot):
    bot.add_plugin(Welcomer(bot))


def unload(bot: lightbulb.Bot):
    bot.remove_plugin("Welcomer")
