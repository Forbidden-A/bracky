#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import asyncio
import logging
import typing

import hikari
import lightbulb

from bracky import config
from bracky.utils import misc, BrackyPlugin


class ReactionMenusPlugin(BrackyPlugin):
    def __init__(self, bot: lightbulb.Bot):
        self.config = config.get_config().role_menus
        super().__init__(bot)

    @lightbulb.listener(hikari.StartedEvent)
    async def _init_menus_on_started(self, _):
        """Checks all menus and makes sure the emojis exist."""
        for menu in self.config:
            try:
                for e in menu.roles:
                    await self.bot.rest.add_reaction(menu.channel, menu.message, e)
            except hikari.NotFoundError:
                # Message does not exist so we immediatly skip to the next menu
                continue

    @lightbulb.listener(hikari.GuildReactionAddEvent)
    async def handle_role_addition_on_reaction_add(self, event: hikari.GuildReactionAddEvent):
        menu = misc.find(self.config, lambda m: m.message == event.message_id)
        if menu is None or event.emoji.mention not in menu.roles:
            return

        await self.bot.rest.add_role_to_member(
            event.guild_id,
            event.user_id,
            menu.roles[event.emoji.mention],
            reason=f"Role menu ({event.message_id})",
        )

    @lightbulb.listener(hikari.GuildReactionDeleteEvent)
    async def handle_role_removal_on_reaction_delete(self, event: hikari.GuildReactionDeleteEvent):
        menu = misc.find(self.config, lambda m: m.message == event.message_id)
        if menu is None or event.emoji.mention not in menu.roles:
            return

        await self.bot.rest.remove_role_from_member(
            event.guild_id,
            event.user_id,
            menu.roles[event.emoji.mention],
            reason=f"Role menu ({event.message_id})",
        )


def load(bot: lightbulb.Bot):
    bot.add_plugin(ReactionMenusPlugin(bot))


def unload(bot: lightbulb.Bot):
    bot.remove_plugin("ReactionMenusPlugin")
