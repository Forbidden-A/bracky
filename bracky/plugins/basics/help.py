#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import itertools
import typing

import lightbulb
from hikari import Snowflake

from bracky import utils
from bracky.bot import Bracky
from bracky.utils import BrackyPlugin, RoleAwareCooldownManager

Commandish = typing.Union[lightbulb.Group, lightbulb.Command]


class BrackyHelp:
    _BUCKETS: typing.Final[typing.Dict[typing.Type[lightbulb.Bucket], str]] = {
        lightbulb.ChannelBucket: "per channel",
        lightbulb.UserBucket: "per user",
        lightbulb.Bucket: "globally",
    }
    """
    A mapping of Bucket to str to be used when displaying command cooldowns.
    """

    _NO_HELP_MESSAGE: typing.Final[str] = ">> No help text provided. <<"
    """
    Expressing that no help text was provided.
    """

    def __init__(self, bot: Bracky):
        self.bot: Bracky = bot

    @staticmethod
    async def send_paginated_help(
        text: typing.Union[typing.Sequence[str], lightbulb.utils.StringPaginator],
        ctx: lightbulb.Context,
    ) -> None:
        """
        A small function to start a navigator with the provided text in the current context.
        """

        if isinstance(text, lightbulb.utils.StringPaginator):
            paginator = text
        else:
            paginator = lightbulb.utils.StringPaginator()
            for line in text:
                paginator.add_line(line)

        navigator = lightbulb.utils.StringNavigator(paginator.build_pages())
        await navigator.run(ctx)

    @staticmethod
    async def object_not_found(ctx: lightbulb.Context, name: str) -> None:
        """
        Send a message indicating that the provided name does not point to a valid category or commandish.
        """

        await utils.send_failure(
            ctx.channel,
            f"`{name}` is not a valid command, group, or category.",
        )

    async def resolve_help_arg(
        self, ctx: lightbulb.Context, args: typing.List[str]
    ) -> None:
        """
        Resolve the provided args, sending information about the resolved object.
        """

        if not args:
            await self.send_help_overview(ctx)
            return

        category = self.bot.categories.get(" ".join(args).casefold())

        if category is not None and len(category) > 0:
            await self.send_category_help(ctx, " ".join(args), category)
            return

        command = self.bot.get_command(args[0])

        if isinstance(command, lightbulb.Group):
            for cmd_name in args[1:]:
                if isinstance(command, lightbulb.Group):
                    command = command.get_subcommand(cmd_name)
        if isinstance(command, lightbulb.Command):
            await self.send_command_help(ctx, command)
        else:
            await self.object_not_found(ctx, " ".join(args))

    @staticmethod
    def get_short_help(command: Commandish) -> str:
        """
        Get the first line of the provided commandish's help text or expressing that no text was provided.
        """

        text = lightbulb.get_help_text(command) or BrackyHelp._NO_HELP_MESSAGE

        return text.splitlines()[0]

    @staticmethod
    def get_help(command: lightbulb.Command) -> str:
        """
        Get the provided commandish's help text or expressing that no text was provided.
        """

        return lightbulb.get_help_text(command) or BrackyHelp._NO_HELP_MESSAGE

    async def send_help_overview(self, ctx: lightbulb.Context) -> None:
        """
        Send overview of bot's commands and categories.
        """

        category_commands: typing.Dict[str, typing.List[Commandish]] = {}

        for category, plugins in self.bot.categories.items():
            commands = await lightbulb.filter_commands(
                ctx, set(itertools.chain.from_iterable(p.commands for p in plugins))
            )
            if commands:
                category_commands[category] = commands

        uncategorised_commands = await lightbulb.filter_commands(
            ctx,
            self.bot.commands.difference(
                set(itertools.chain.from_iterable(category_commands.values()))
            ),
        )

        category_commands["uncategorised"] = uncategorised_commands

        paginator = lightbulb.utils.pag.StringPaginator(
            prefix=">>> ```adoc\n", suffix="```"
        )

        paginator.add_line("==== Bot help ====\n")
        paginator.add_line(
            f"Use `{ctx.clean_prefix}help [category|command]` for more information.\n"
        )
        paginator.add_line("==== Categories ====")

        for category in category_commands.keys():
            paginator.add_line(f"- {category.capitalize()}")

        for category, commands in category_commands.items():
            paginator.new_page()
            paginator.add_line(f"==== {category.capitalize()} ====\n")

            for c in sorted(commands, key=lambda cmd: cmd.qualified_name):
                short_help = self.get_short_help(c)
                paginator.add_line(f"== {c.qualified_name}")
                paginator.add_line(f"- {short_help}")

        await self.send_paginated_help(paginator, ctx)

    def get_role_name_from_id(self, role_id: Snowflake) -> typing.Optional[str]:
        """
        Get the name of the role with the provided id.
        """

        role = self.bot.cache.get_role(role_id)

        if role is None:
            return None

        return role.name

    async def send_command_help(
        self,
        ctx: lightbulb.Context,
        command: Commandish,
    ) -> None:
        """
        Send help for the provided commandish.
        """

        is_group = isinstance(command, lightbulb.Group)
        signature = f"{ctx.clean_prefix}{lightbulb.get_command_signature(command)}"
        type_ = "group" if is_group else "command"

        paginator = lightbulb.utils.pag.StringPaginator(
            prefix=">>> ```adoc\n", suffix="```"
        )

        paginator.add_line(f"=== Help for {command.qualified_name} {type_} ===\n")

        paginator.add_line(signature)
        paginator.add_line("-" * len(signature))
        paginator.add_line("")

        paginator.add_line(f"* {self.get_help(command)}\n")

        if command.aliases:
            paginator.add_line("[Aliases]")

            for alias in command.aliases:
                paginator.add_line(f"- {alias}")

        if command.cooldown_manager is not None:
            bucket = (
                BrackyHelp._BUCKETS.get(command.cooldown_manager.bucket)
                or command.cooldown_manager.bucket.__name__
            )
            paginator.add_line("[Cooldown]")
            paginator.add_line(f"* Usages: {command.cooldown_manager.usages}")
            paginator.add_line(f"* Length: {command.cooldown_manager.length}s")
            paginator.add_line(f"* Bucket: {bucket}")

            if isinstance(command.cooldown_manager, RoleAwareCooldownManager):
                paginator.add_line("* Excluded roles:")
                excluded_roles = (
                    name
                    for r in command.cooldown_manager.roles
                    if (name := self.get_role_name_from_id(r)) is not None
                )

                for role in excluded_roles:
                    paginator.add_line(f"- {role}")

        if isinstance(command, lightbulb.Group):
            if command.subcommands:
                paginator.add_line("[Subcommands]")

                for c in sorted(
                    command.subcommands, key=lambda cmd: cmd.qualified_name
                ):
                    short_help = self.get_short_help(c)
                    paginator.add_line(f"* {c.qualified_name} - {short_help}")

        await self.send_paginated_help(paginator, ctx)

    async def send_category_help(
        self,
        ctx: lightbulb.Context,
        name: str,
        children: typing.List[lightbulb.Plugin],
    ):
        """
        Send help about the provided category.
        """

        paginator = lightbulb.utils.pag.StringPaginator(
            prefix=">>> ```adoc\n", suffix="```"
        )

        paginator.add_line(f"=== Commands in {name.capitalize()} category ===\n")

        all_category_commands = itertools.chain.from_iterable(
            p.commands for p in children
        )

        for line in (
            f"{c.qualified_name} :: {self.get_short_help(c)}"
            for c in all_category_commands
        ):
            paginator.add_line(line)

        await self.send_paginated_help(paginator, ctx)


class Help(BrackyPlugin):
    def __init__(self, bot: Bracky):
        # noinspection PyProtectedMember
        self._backup_help_command = bot._help_impl
        bot._help_impl = BrackyHelp(bot)
        bot.remove_command("help")
        super().__init__(bot=bot)

    @lightbulb.command(name="help")
    async def _help_cmd(self, ctx: lightbulb.Context) -> None:
        """
        Displays help for the bot, a command, or a category.

        If no argument is specified with the command then a help menu
        for the bot as a whole is displayed instead.
        """
        arg = (
            ctx.message.content[len(f"{ctx.prefix}{ctx.invoked_with}") :]
            .strip()
            .split()
        )
        await ctx.bot.help_command.resolve_help_arg(ctx, arg)

    def plugin_remove(self):
        self.bot._help_impl = self._backup_help_command


def load(bot):
    bot.add_plugin(Help(bot=bot))


def unload(bot):
    bot.remove_plugin("Help")
