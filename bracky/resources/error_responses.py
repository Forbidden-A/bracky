#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>

import difflib
import typing

import hikari
import lightbulb


Action = typing.Callable[[lightbulb.CommandErrorEvent], typing.Awaitable[None]]


# Utility methods for quicker and prettier response definition
def reply(text: str) -> Action:
    """
    Returns a coroutine function that replies with the given text in a quote,
    formatting it with `event` as the received CommandErrorEvent.
    """

    async def inner(event: lightbulb.CommandErrorEvent):
        await event.message.respond(f">>> {text.format(event=event)}")

    return inner


def react(emoji: hikari.Emojiish) -> Action:
    """Returns a coroutine function that reacts with the given emoji."""

    async def inner(event: lightbulb.CommandErrorEvent):
        await event.message.add_reaction(emoji)

    return inner


async def suggest_command(event: lightbulb.CommandErrorEvent) -> None:
    """Tries to find the best match for similarly named commands, and presents it to the user."""
    matches = difflib.get_close_matches(
        event.exception.args[0],
        event.app.command_names,
    )

    if not matches:
        await react("\N{BLACK QUESTION MARK ORNAMENT}")(event)
        return

    await reply(
        f"\N{BLACK QUESTION MARK ORNAMENT} Command not found. Did you mean `{matches[0]}`?"
    )(event)


RESPONSES: typing.Final[typing.Dict[typing.Type[BaseException], Action]] = {
    lightbulb.errors.CommandNotFound: suggest_command,
    lightbulb.errors.CommandSyntaxError: reply("Invalid command syntax!"),
    lightbulb.errors.TooManyArguments: reply("You passed too many arguments!"),
    lightbulb.errors.NotEnoughArguments: reply(
        "You are missing the `{event.missing_args}` arguments."
    ),
    lightbulb.errors.OnlyInGuild: reply("This command only works in the server."),
    lightbulb.errors.OnlyInDM: reply("This command only works in DMs."),
    lightbulb.errors.MissingRequiredRole: reply("You are not authorised to run this command!"),
}
